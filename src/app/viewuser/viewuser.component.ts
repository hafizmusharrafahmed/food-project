import { Component, OnInit } from '@angular/core';


 class Card {
  id: any;
  email: any;
}
@Component({
  selector: 'app-viewuser',
  templateUrl: './viewuser.component.html',
  styleUrls: ['./viewuser.component.css']

})
// // tslint:disable-next-line:component-class-suffix

export class ViewuserComponent implements OnInit {
  newcard: Card;
  constructor() {
    this.newcard = new Card();
  }
  value = [
    {
      'id': '5',
      'email': 'email@.com'
    }
  ];

// card: Card = {
//     id: '',
//     email: ''
//   };



  ngOnInit() {

  }

   add() {
     if (this.newcard) {
      // tslint:disable-next-line:no-var-keyword
      // tslint:disable-next-line:prefer-const
      let entry = {
         'id' : this.newcard.id,
         'email': this.newcard.email
       };
       this.value.push(entry);
     }
   }

}
