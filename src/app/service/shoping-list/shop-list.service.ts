import {Injectable} from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Item } from '../../model/item/item';


@Injectable()
export class ShopListService {

    private shoppinglistref = this.db.list<Item>
    ('shopping-list');
    constructor(private db: AngularFireDatabase) {}
        getShoppingList() {

            return this.shoppinglistref;

        }
            addItem(item: Item) {
            return this.shoppinglistref.push(item);

        }
    }
