import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {NoopAnimationsModule} from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { ViewuserComponent } from './viewuser/viewuser.component';
import { UserComponent } from './user/user.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FormsModule , FormBuilder, ReactiveFormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule, Routes} from '@angular/router';
import { HomeComponent } from './home/home.component';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import { Firebase } from './firebase';
import { AngularFireDatabaseModule} from 'angularfire2/database';

import { AngularFireModule} from 'angularfire2';
import { ShopListService } from './service/shoping-list/shop-list.service';


const appRoutes: Routes =  [
  {path: 'user', component: UserComponent},
  {path: 'home', component: HomeComponent},

  {path: 'View', component: ViewuserComponent},
  {path: 'navbar', component: NavbarComponent}
];


@NgModule({
  declarations: [
    AppComponent,
    ViewuserComponent,
    UserComponent,
    NavbarComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
   ReactiveFormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    MatInputModule,
    MatButtonModule,
    NoopAnimationsModule,
    AngularFireModule.initializeApp(Firebase),
    AngularFireDatabaseModule
  ],
  providers: [ShopListService],
  bootstrap: [AppComponent]
})
export class AppModule { }
