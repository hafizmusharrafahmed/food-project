import { Component, OnInit } from '@angular/core';
import { Item } from '../model/item/item';
import { Jsonp } from '@angular/http/src/http';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ShopListService } from '../service/shoping-list/shop-list.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})

export class UserComponent implements OnInit {
  public total= 150;
  public sum = 0;
  public  fine = 0;
  form: FormGroup;
  item: Item = {
    first : '',
    address: '',
    meal: '',
    city: '',
    quantity: '',
    num: undefined
};
  constructor(private value: Router,
    private formBuilder: FormBuilder,
    private shopping: ShopListService) { }
  ngOnInit() {
    this.form = this.formBuilder.group({
      first: ['', Validators.required],
      address: ['', Validators.required],
      meal: ['', Validators.required],
      city: ['', Validators.required],
      quantity: ['', Validators.required],
      num: ['', Validators.required]
    });
  }
  post(amount) {
        this.fine = this.total * this.item.quantity;
    alert('thanks the total amount of your order is ' + JSON.stringify(this.fine));
    this.value.navigate(['home']);
  }
  addItem(item: Item) {
    this.shopping.addItem(item).then(ref => {
      this.value.navigate(['home']);
    });
  }
}
